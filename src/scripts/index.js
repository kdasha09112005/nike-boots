import "../styles/style.css";
import { setUpHeader } from "../components/header.js";
import { setUpMain } from "../components/main.js";
import { setUpFooter } from "../components/footer.js";

const app = document.querySelector("#app");

setUpHeader(app);

setUpMain(app);

setUpFooter(app);
