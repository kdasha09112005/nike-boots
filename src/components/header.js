import { createElement } from "../scripts/createElement.js";
import logo from "../assets/nike-logo.png";
import facebook from "../assets/facebook.png";
import twitter from "../assets/twitter.png";
import instagram from "../assets/instagram.png";
import "./header.css";

export function setUpHeader(parent) {
  createElement({
    tag: "header",
    attributes: {
      class: "app__header",
    },
    parent,
  });

  const header = document.querySelector(".app__header");

  createElement({
    tag: "img",
    attributes: {
      class: "header__image",
      src: logo,
    },
    parent: header,
  });

  createElement({
    tag: "p",
    attributes: {
      class: "header__info",
    },
    parent: header,
    content: "WOOCOMMERCE  PRODUCT  SLIDER",
  });

  createElement({
    tag: "ul",
    attributes: {
      class: "header__menu",
    },
    parent: header,
  });

  const menu = document.querySelector(".header__menu");

  createElement({
    tag: "img",
    attributes: {
      class: "header__icon",
      src: twitter,
    },
    parent: menu,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "header__icon",
      src: instagram,
    },
    parent: menu,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "header__icon header__icon-facebook",
      src: facebook,
    },
    parent: menu,
  });

  createElement({
    tag: "p",
    attributes: {
      class: "header__info",
    },
    parent: header,
    content: "CART (0)",
  });
}
