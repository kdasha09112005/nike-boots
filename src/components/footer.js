import "./footer.css";
import { createElement } from "../scripts/createElement.js";
import white__star from "../assets/white-star.png";
import empty__star from "../assets/empty-star.png";

export function setUpFooter(parent) {
  createElement({
    tag: "footer",
    attributes: {
      class: "app__footer",
    },
    parent,
  });

  const footer = document.querySelector(".app__footer");

  createElement({
    tag: "div",
    attributes: {
      class: "footer__block-info",
    },
    parent: footer,
  });

  const footer__block__info = document.querySelector(".footer__block-info");

  createElement({
    tag: "p",
    attributes: {
      class: "footer__info__heading",
    },
    parent: footer__block__info,
    content: "$749",
  });

  createElement({
    tag: "div",
    attributes: {
      class: "footer__rating",
    },
    parent: footer__block__info,
  });

  const footer__rating = document.querySelector(".footer__rating");

  createElement({
    tag: "img",
    attributes: {
      class: "footer__icon",
      src: white__star,
    },
    parent: footer__rating,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "footer__icon",
      src: white__star,
    },
    parent: footer__rating,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "footer__icon",
      src: white__star,
    },
    parent: footer__rating,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "footer__icon",
      src: white__star,
    },
    parent: footer__rating,
  });

  createElement({
    tag: "img",
    attributes: {
      class: "footer__icon",
      src: empty__star,
    },
    parent: footer__rating,
  });

  createElement({
    tag: "p",
    attributes: {
      class: "footer__info",
    },
    parent: footer__block__info,
    content:
      "A Woocommerce product gallery Slider for Slider Revolution with mind-blowing visuals.",
  });

  createElement({
    tag: "div",
    attributes: {
      class: "footer__block-cost",
    },
    parent: footer,
  });

  const camelCase = document.querySelector(".footer__block-cost");

  createElement({
    tag: "p",
    attributes: {
      class: "footer__info__underline",
    },
    parent: camelCase,
    content: "BUY NOW",
  });
}
