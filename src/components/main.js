import "./main.css";
import { createElement } from "../scripts/createElement.js";
import nike__boot from "../assets/nike-boot.png";

export function setUpMain(parent) {
  createElement({
    tag: "main",
    attributes: {
      class: "app__main",
    },
    parent,
  });

  const main = document.querySelector(".app__main");

  createElement({
    tag: "span",
    attributes: {
      class: "main__arrow material-symbols-outlined",
    },
    parent: main,
    content: "arrow_back",
  });

  createElement({
    tag: "div",
    attributes: {
      class: "main__image-boot",
    },
    parent: main,
  });

  const main__image__boot = document.querySelector(".main__image-boot");

  createElement({
    tag: "img",
    attributes: {
      class: "main__image",
      src: nike__boot,
    },
    parent: main__image__boot,
  });

  createElement({
    tag: "div",
    attributes: {
      class: "main__image__shadow",
    },
    parent: main__image__boot,
  });

  createElement({
    tag: "h1",
    attributes: {
      class: "main__heading main__heading__bold",
    },
    parent: main,
    content: "NIKE CRYPTOKICK",
  });

  createElement({
    tag: "h1",
    attributes: {
      class: "main__heading",
    },
    parent: main,
    content: "NIKE CRYPTOKICK",
  });

  createElement({
    tag: "span",
    attributes: {
      class: "main__arrow material-symbols-outlined",
    },
    parent: main,
    content: "arrow_forward",
  });
}
